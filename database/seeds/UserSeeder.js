'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const User = use('App/Models/User')

class UserSeeder {
  async run() {
    const demo1 = new User()
    demo1.username = 'demosatu'
    demo1.password = 'password'
    demo1.email = 'demo1@mail.com'
    await demo1.save()
    const demo2 = new User()
    demo2.username = 'demodua'
    demo2.password = 'password'
    demo2.email = 'demo2@mail.com'
    await demo2.save()
  }
}

module.exports = UserSeeder
