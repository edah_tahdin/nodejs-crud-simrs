'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ReferrerListTimestampSchema extends Schema {
  async up() {
    await this.db.raw(`
      CREATE TRIGGER referrer_lists
      BEFORE UPDATE
      ON referrer_lists
      FOR EACH ROW
      EXECUTE PROCEDURE upd_timestamp_users();
    `)
  }

  down() {}
}

module.exports = ReferrerListTimestampSchema
