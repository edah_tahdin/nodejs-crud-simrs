'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ReferrerSchema extends Schema {
  up () {
    this.create('referrers', (table) => {
      table.uuid('referrers_id').unique().defaultTo(this.db.raw('public.gen_random_uuid()'))
      table.string('referrers_name', 128).notNullable()
      table.timestamp('created_at').defaultTo('NOW()')
      table.timestamp('updated_at').defaultTo('NOW()')
      table.timestamp('deleted_at')
    })
  }

  down () {
    this.drop('referrers')
  }
}

module.exports = ReferrerSchema
