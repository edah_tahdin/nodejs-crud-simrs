'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SetupSchema extends Schema {
  async up() {
    await this.db.raw(`CREATE EXTENSION IF NOT EXISTS pgcrypto;`)
    await this.db.raw(`
      CREATE OR REPLACE FUNCTION upd_timestamp_users() 
      RETURNS TRIGGER AS $$ 
      BEGIN 
        NEW.updated_at = NOW(); 
        RETURN NEW; 
        END;
      $$ LANGUAGE plpgsql;
    `)
  }

  async down() {
    await this.db.raw(`DROP EXTENSION IF EXISTS pgcrypto;`)
    await this.db.raw(`DROP FUNCTION IF EXISTS upd_timestamp_users();`)
  }
}

module.exports = SetupSchema
