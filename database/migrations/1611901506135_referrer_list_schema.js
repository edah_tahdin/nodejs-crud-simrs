'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ReferrerListSchema extends Schema {
  up() {
    this.create('referrer_lists', (table) => {
      table.uuid('referrer_lists_id').unique().defaultTo(this.db.raw('public.gen_random_uuid()'))
      table.uuid('referrer_lists_referrer_id').notNullable().references('referrers_id').inTable('referrers')
      table.string('referrer_lists_name', 128).notNullable()
      table.string('referrer_lists_contact', 24).notNullable()
      table.text('referrer_lists_address').notNullable()
      table.string('referrer_lists_ppk_code', 16).notNullable()
      table.string('referrer_lists_facility_code', 16).notNullable()
      table.timestamp('created_at').defaultTo('NOW()')
      table.timestamp('updated_at').defaultTo('NOW()')
      table.timestamp('deleted_at').nullable()
    })
  }

  down() {
    this.drop('referrer_lists')
  }
}

module.exports = ReferrerListSchema
