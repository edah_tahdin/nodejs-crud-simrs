'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ReferrerTimestampSchema extends Schema {
  async up() {
    await this.db.raw(`
      CREATE TRIGGER referrers
      BEFORE UPDATE
      ON referrers
      FOR EACH ROW
      EXECUTE PROCEDURE upd_timestamp_users();
    `)

    await this.db.raw(`
      INSERT INTO referrers (referrers_name) VALUES
      ('Bidan'),
      ('Diri Sendiri'),
      ('Dokter Gigi'),
      ('Dokter Praktek'),
      ('Klinik'),
      ('Puskesmas'),
      ('Rumah Sakit Pemerintah'),
      ('Rumah Sakit Swasta'),
      ('Dll');
    `)
  }

  down() {
    this.db.raw(`DELETE FROM public.referrers`)
  }
}

module.exports = ReferrerTimestampSchema
