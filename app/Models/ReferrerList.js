'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ReferrerList extends Model {
  static get table() {
    return 'referrer_lists'
  }
  static get primaryKey() {
    return 'referrer_lists_id'
  }
}

module.exports = ReferrerList
