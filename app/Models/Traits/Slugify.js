'use strict'

class Slugify {
  register (Model, options) {
    Model.queryMacro('whereSlug', function (value) {
      this.where('referrers_name', value)
      return this
    })
  }
}

module.exports = Slugify
