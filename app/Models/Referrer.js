'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Referrer extends Model {
  static get table() {
    return 'referrers'
  }

  static get primaryKey() {
    return 'referrers_id'
  }

  static boot () {
    super.boot()
    this.addTrait('SoftDelete', {deletedAtColumn: 'deleted_at'})
    // this.addTrait('@provider:Lucid/SoftDeletes', {
    //   deletedAtColumn: 'deleted_at'
    // })
  }

}

module.exports = Referrer
