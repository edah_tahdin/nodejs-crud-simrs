'use strict'
const ReferrerList = use('App/Models/ReferrerList')
const Redis = use('Redis')

class ReferrerListController {
  results = {}

  async index({
    request,
    response
  }) {
    const expiredTime = 600

    const page = parseInt(request.get().page) || 1
    const limit = parseInt(request.get().limit) || 20
    const offset = parseInt(limit * (page - 1))
    const keyword = request.get().keyword || false
    const sort = request.get().sort || false

    let total = parseInt(await ReferrerList.query().getCount())
    let pages = Math.ceil(total / limit)

    let meta = {
      total,
      limit,
      page,
      pages,
      sort,
      keyword
    }

    const isUpdate = await Redis.get('update')
    const cachedReferrerList = await Redis.get('referrer_lists:' + JSON.stringify(meta))

    if (cachedReferrerList && request.get() && isUpdate === 'false') {
      let cachedData = JSON.parse(cachedReferrerList)
      let referrerLists = cachedData.data
      this.results.status = true
      this.results.message = referrerLists.length + ' data ditemukan di cache'
      this.results.meta = meta
      this.results.properties = cachedData.properties
      this.results.data = referrerLists
    } else {

      try {
        // let query = ReferrerList.query().offset(offset).limit(limit)
        let query = ReferrerList.query().select('referrer_lists.*', 'referrers_name').innerJoin('referrers', 'referrers_id', 'referrer_lists_referrer_id').offset(offset).limit(limit)
        if (keyword) query.where('referrers_name', 'ILIKE', '%' + keyword + '%')

        if (sort) {
          let arr = sort.split(',')
          arr.forEach((item) => {
            let order = item.substring(0, item.indexOf("("))
            let field = item.match(/\((.*)\)/)[1]
            query.orderBy(field, order)
          })
        }
        
        let referrerLists = await query.fetch()

        let set_at = new Date()
        let expired_at = set_at.setSeconds(set_at.getSeconds() + expiredTime);

        let cacheReferrerList = {
          properties: {
            set_at,
            expired_at
          },
          data: referrerLists
        }

        await Redis.setex('referrer_lists:' + JSON.stringify(meta), expiredTime, JSON.stringify(cacheReferrerList))
        await Redis.set('update', false)

        this.results.status = true
        this.results.message = referrerLists.rows.length + ' data ditemukan di database'
        this.results.meta = meta
        this.results.properties = cacheReferrerList.properties
        this.results.data = referrerLists

      } catch (error) {
        this.results.status = false
        this.results.message = error.message || 'Terjadi kesalahan dalam mendapatkan data'
        this.results.data = null
      }

    }
    return response.json(this.results)
  }

  async show({
    params,
    response
  }) {
    const referrerList = await ReferrerList.find(params.id)

    this.results.status = referrerList ? true : false
    this.results.message = referrerList ? 'Data ditemukan.' : 'Data tidak ditemukan.'
    this.results.data = referrerList || null

    return response.json(this.results)
  }

  async store({
    request,
    response
  }) {
    try {
      const referrerList = new ReferrerList()
      referrerList.referrer_lists_referrer_id = request.body.referrer_lists_referrer_id
      referrerList.referrer_lists_name = request.body.referrer_lists_name
      referrerList.referrer_lists_contact = request.body.referrer_lists_contact
      referrerList.referrer_lists_address = request.body.referrer_lists_address
      referrerList.referrer_lists_ppk_code = request.body.referrer_lists_ppk_code
      referrerList.referrer_lists_facility_code = request.body.referrer_lists_facility_code
      await referrerList.save()

      this.results.status = true
      this.results.message = 'Data berhasil ditambahkan.'
      this.results.data = request.body

      await Redis.set('update', true)

      return response.status(201).json(this.results)
    } catch (error) {
      this.results.status = false
      this.results.message = error.message || 'Data gagal ditambahkan.'
      this.results.data = null

      return response.status(500).json(this.results)
    }
  }

  async update({
    params,
    request,
    response
  }) {
    const referrerList = await ReferrerList.find(params.id)
    if (!referrerList) {
      this.results.status = false
      this.results.message = 'Data tidak ditemukan.'

      return response.status(404).json(this.results)
    }
    referrerList.referrers_id = request.body.referrers_id
    referrerList.referrer_lists_name = request.body.referrer_lists_name
    referrerList.referrer_lists_contact = request.body.referrer_lists_contact
    referrerList.referrer_lists_address = request.body.referrer_lists_address
    referrerList.referrer_lists_ppk_code = request.body.referrer_lists_ppk_code
    referrerList.referrer_lists_facility_code = request.body.referrer_lists_facility_code
    await referrerList.save()

    await Redis.set('update', true)

    this.results.status = true
    this.results.message = 'Data berhasil diupdate.'
    this.results.data = referrerList

    return response.status(200).json(this.results)
  }

  async destroy({
    params,
    response
  }) {
    const referrerList = await ReferrerList.find(params.id)
    if (!referrerList) {
      this.results.status = false
      this.results.message = 'Data tidak ditemukan.'

      return response.status(404).json(this.results)
    }
    await referrerList.delete()

    await Redis.set('update', true)

    this.results.status = true
    this.results.message = 'Data berhasil dihapus.'
    this.results.data = referrerList

    return response.status(200).json(this.results)
  }
}

module.exports = ReferrerListController
