'use strict'
const {
  validate
} = use('Validator')

class AuthController {
  async login({
    auth,
    request
  }) {
    const {
      refreshToken,
      email,
      password
    } = request.post();
    if (refreshToken) {
      return await auth.
      generateForRefreshToken(refreshToken);
    }
    return auth.withRefreshToken().attempt(email, password);
  }

  async refresh({
    auth,
    request
  }) {
    const {
      refreshToken
    } = request.post();

    return await auth.generateForRefreshToken(refreshToken);
  }

  async getUser({
    response,
    auth
  }) {
    try {
      const user = await auth.getUser()

      return {
        id: user.id,
        username: user.username,
        email: user.email
      }
    } catch (error) {
      response.status(401).send(error)
    }
  }
}

module.exports = AuthController
