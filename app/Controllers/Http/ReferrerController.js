'use strict'
const Referrer = use('App/Models/Referrer')
const Redis = use('Redis')

class ReferrerController {
  results = {}

  async index({
    request,
    response
  }) {
    try {
    } catch (error) {
      return response.json(console.log)
    }
    const expiredTime = 600

    const page = parseInt(request.get().page) || 1
    const limit = parseInt(request.get().limit) || 20
    const offset = parseInt(limit * (page - 1))
    const keyword = request.get().keyword || false
    const sort = request.get().sort || false

    let total = parseInt(await Referrer.query().getCount())
    let pages = Math.ceil(total / limit)

    let meta = {
      total,
      limit,
      page,
      pages,
      sort,
      keyword
    }

    const isUpdate = await Redis.get('update')
    const cachedReferrer = await Redis.get('referrers:' + JSON.stringify(meta))

    if (cachedReferrer && request.get() && isUpdate === 'false') {
      let cachedData = JSON.parse(cachedReferrer)
      let referrers = cachedData.data
      this.results.status = true
      this.results.message = referrers.length + ' data ditemukan di cache'
      this.results.meta = meta
      this.results.properties = cachedData.properties
      this.results.data = referrers
    } else {

      try {
        let query = Referrer.query().offset(offset).limit(limit)
        if (keyword) query.where('referrers_name', 'ILIKE', '%' + keyword + '%')

        if (sort) {
          let arr = sort.split(',')
          arr.forEach((item) => {
            let order = item.substring(0, item.indexOf("("))
            let field = item.match(/\((.*)\)/)[1]
            query.orderBy(field, order)
          })
        }
        
        let referrers = await query.fetch()

        let set_at = new Date()
        let expired_at = set_at.setSeconds(set_at.getSeconds() + expiredTime);

        let cacheReferrer = {
          properties: {
            set_at,
            expired_at
          },
          data: referrers
        }

        await Redis.setex('referrers:' + JSON.stringify(meta), expiredTime, JSON.stringify(cacheReferrer))
        await Redis.set('update', false)

        this.results.status = true
        this.results.message = referrers.rows.length + ' data ditemukan di database'
        this.results.meta = meta
        this.results.properties = cacheReferrer.properties
        this.results.data = referrers

      } catch (error) {
        this.results.status = false
        this.results.message = error.message || 'Terjadi kesalahan dalam mendapatkan data'
        this.results.data = null
      }

    }
    return response.json(this.results)
  }

  async show({
    params,
    response
  }) {
    const referrer = await Referrer.find(params.id)

    this.results.status = referrer ? true : false
    this.results.message = referrer ? 'Data ditemukan.' : 'Data tidak ditemukan.'
    this.results.data = referrer || null

    return response.json(this.results)
  }

  async store({
    request,
    response
  }) {
    try {
      const referrer = new Referrer()
      referrer.referrers_name = request.body.referrers_name
      await referrer.save()

      this.results.status = true
      this.results.message = 'Data berhasil ditambahkan.'
      this.results.data = request.body

      await Redis.set('update', true)

      return response.status(201).json(this.results)
    } catch (error) {
      this.results.status = false
      this.results.message = error.message || 'Data gagal ditambahkan.'
      this.results.data = null

      return response.status(500).json(this.results)
    }
  }

  async update({
    params,
    request,
    response
  }) {
    const referrer = await Referrer.find(params.id)
    if (!referrer) {
      this.results.status = false
      this.results.message = 'Data tidak ditemukan.'

      return response.status(404).json(this.results)
    }
    referrer.referrers_name = request.body.referrers_name
    await referrer.save()

    await Redis.set('update', true)

    this.results.status = true
    this.results.message = 'Data berhasil diupdate.'
    this.results.data = referrer

    return response.status(200).json(this.results)
  }

  async destroy({
    params,
    response
  }) {
    const referrer = await Referrer.find(params.id)
    if (!referrer) {
      this.results.status = false
      this.results.message = 'Data tidak ditemukan.'

      return response.status(404).json(this.results)
    }
    await referrer.delete()

    await Redis.set('update', true)

    this.results.status = true
    this.results.message = 'Data berhasil dihapus.'
    this.results.data = referrer

    return response.status(200).json(this.results)
  }

  async restore({
    params,
    response
  }) {
    const referrer = await Referrer.find(params.id)
    
    await referrer.restore()

    await Redis.set('update', true)

    this.results.status = true
    this.results.message = 'Data berhasil direstore.'
    this.results.data = referrer

    return response.status(200).json(this.results)
  }
}

module.exports = ReferrerController
