'use strict'

class ReferrerList {
  get rules() {
    return {
      referrer_lists_referrer_id: 'required',
      referrer_lists_name: 'required',
      referrer_lists_contact: 'required',
      referrer_lists_address: 'required',
      referrer_lists_ppk_code: 'required',
      referrer_lists_facility_code: 'required',
    }
  }

  get messages() {
    return {
      'referrer_lists_referrer_id.required': 'Referrer name dibutuhkan.',
      'referrer_lists_name.required': 'Referrer list name dibutuhkan.',
      'referrer_lists_contact.required': 'Kontak dibutuhkan.',
      'referrer_lists_address.required': 'Alamat dibutuhkan.',
      'referrer_lists_ppk_code.required': 'Kode PPK dibutuhkan.',
      'referrer_lists_facility_code.required': 'Kode fasilitas dibutuhkan.',
    }
  }

  async fails(errorMessages) {
    return this.ctx.response.send(errorMessages);
  }
}

module.exports = ReferrerList
