'use strict'

class Auth {
  get rules() {
    return {
      email: 'required|email',
      password: 'required',
    }
  }

  get messages() {
    return {
      'email.required': 'Email dibutuhkan.',
      'email.email': 'Email tidak valid.',
      'password.required': 'Password dibutuhkan.'
    }
  }

  get sanitizationRules() {
    return {
      email: 'normalize_email',
    };
  }

  async fails(errorMessages) {
    return this.ctx.response.send(errorMessages);
  }
}

module.exports = Auth
