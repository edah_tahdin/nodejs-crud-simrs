'use strict'

class Referrer {
  get rules() {
    return {
      referrers_name: 'required'
    }
  }

  get messages() {
    return {
      'referrers_name.required': 'Referrer name dibutuhkan.',
    }
  }

  async fails(errorMessages) {
    return this.ctx.response.send(errorMessages);
  }
}

module.exports = Referrer
