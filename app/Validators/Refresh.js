'use strict'

class Refresh {
  get rules() {
    return {
      refreshToken: 'required'
    }
  }

  get messages() {
    return {
      'refreshToken.required': 'Refresh token dibutuhkan.',
    }
  }

  async fails(errorMessages) {
    return this.ctx.response.send(errorMessages);
  }
}

module.exports = Refresh
