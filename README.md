# Simple REST API CRUD

## Setup

Install additional package

1. Postgresql

```bash
npm i pg --save
```

2. Validator

```bash
adonis install @adonisjs/validator
```

3. Redis

```bash
adonis install @adonisjs/redis
```

4. SoftDelete

```bash
adonis install @radmen/adonis-lucid-soft-deletes
```

### Migrations

Run the following command to run startup migrations.

```js
adonis migration:run
```

#### Tested

1. NodeJs = 14.15.4
2. npm = 6.14.10
3. AdonisJs = 4.1
4. Redis = 6.0.10