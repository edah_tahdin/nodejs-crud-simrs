'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return {
    greeting: 'Hello world in JSON'
  }
})

Route.group(() => {
    Route.post('/login', 'AuthController.login').validator('Auth')
    Route.get('/user', 'AuthController.getUser').middleware(['auth'])
    Route.post('/refresh', 'AuthController.refresh').validator('Refresh')
  })
  .prefix('api/auth')

Route.group(() => {
  Route.patch('referrers/:id', 'ReferrerController.restore')
  Route.resource('referrers', 'ReferrerController').validator(new Map([
    [
      ['referrers.store'],
      ['Referrer']
    ],
    [
      ['referrers.update'],
      ['Referrer']
    ]
  ]))

  Route.patch('referrer_lists/:id', 'ReferrerController.restore')
  Route.resource('referrer_lists', 'ReferrerListController').validator(new Map([
    [
      ['referrer_lists.store'],
      ['ReferrerList']
    ],
    [
      ['referrer_lists.update'],
      ['ReferrerList']
    ]
  ]))
}).prefix('api/v1').middleware('auth')
